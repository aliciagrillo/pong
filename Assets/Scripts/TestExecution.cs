﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestExecution : MonoBehaviour
{
    // Start is called before the first frame update
    public int NumeroDeTest = 0;

    void Awake()
    {
        Debug.Log("Estoy en el Awake de TestExecutionOrder" + NumeroDeTest);
    }

    // Update is called once per frame
    private void OnEnable()
    {
        Debug.LogWarning("Activo TestExecutionOrder" + NumeroDeTest);
    }

    private void Start()
    {
        Debug.LogError("Activo TestExecutionOrder" + NumeroDeTest);
    }

    void Update()
    {
        Debug.LogWarning("Activo TestExecutionOrder" + NumeroDeTest);
    }

    private void LateUpdate()
    {
        Debug.LogError("Activo TestExecutionOrder" + NumeroDeTest);
    }
}