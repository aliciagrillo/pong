﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Vector2 ballPosition;
    public Vector2 ballVelocity;
    public float maxY;
    public float maxX;

    void Awake()
    {
        ballPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        ballPosition.y = transform.position.y + ballVelocity.y * Time.deltaTime;
        ballPosition.x = transform.position.x + ballVelocity.x * Time.deltaTime;

        if (ballPosition.y > maxY || ballPosition.y < -maxY)
        {
            ballVelocity.y = -ballVelocity.y;
        }

        if (ballPosition.x > maxX || ballPosition.x < -maxX)
        {
            ballVelocity.x = -ballVelocity.x;
        }


        transform.position = new Vector3(ballPosition.x, ballPosition.y, transform.position.z);
    }

}